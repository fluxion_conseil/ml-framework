import importlib
import json

def load_model(model_name):
    "Load a model from the models folder"
    config = importlib.import_module(f".{model_name}", "models")
    model = config.model
    fit_params = config.fit_params
    return model, fit_params

def load_json(filename):
    with open(filename) as f:
        return(json.load(f))