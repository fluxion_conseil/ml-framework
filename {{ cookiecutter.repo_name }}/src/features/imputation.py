import pandas as pd
import numpy as np

from sklearn.impute import SimpleImputer

def _imputer(df, cols_with_missing, strategy=None):
    """Impute a value for missing values. Wrapper of the SimpleImputer sklearn class.
    
    https://scikit-learn.org/stable/modules/generated/sklearn.impute.SimpleImputer.html
    
    Args:
        df_subset: Pandas DataFrame
            A subset of df columns with a unique type.
        cols_with_missing: str or list
            The list of columns that contains missing values.
        strategy: str   
            The imputation strategy.
            If “mean”, then replace missing values using the mean along each column. Can only be used with numeric data.
            If “median”, then replace missing values using the median along each column. Can only be used with numeric data.
            If “most_frequent”, then replace missing using the most frequent value along each column. Can be used with strings or numeric data.
            If “constant”, then replace missing values with fill_value. Can be used with strings or numeric data.
    """
    df = df[cols_with_missing].copy(deep=True)
    # Numerical
    numerical = df.loc[:, df.apply(is_numeric_dtype)]  

    if cols_type == 'categorical' and strategy is None:
        imp = SimpleImputer(missing_values=np.nan, strategy='mode')
    elif cols_type == 'numerical' and strategy is None:
        imp = SimpleImputer(missing_values=np.nan, strategy='mean')
    else:
        imp = SimpleImputer(missing_values=np.nan, strategy=strategy)
    for col in cols:
        df[[col]] = imp.fit(df[[col]])
    return df