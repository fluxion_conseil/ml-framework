import pandas as pd
import numpy as np
import click

from utils import load_model, load_json
from models.metrics import metrics

from pathlib import Path
from sklearn.model_selection import cross_val_predict
from datetime import datetime as dt

## Helpers function
def _calculate_metrics(df, metrics):
    """Calculate metrics on the outputed dataset"""
    output = pd.Series({
        key: np.round(metric(df['y_true'], df['y_hat']), 2) for key, metric in metrics.items()
    })
    return output

def _prepare_input(df, target):
    """Transform pandas DataFrame into X, y format"""
    if target not in df.columns:
        raise KeyError(f"{target} must be a columns of the DataFrame")
    y = df[target]
    X = df.drop(target, axis=1)
    return X, y

def _load_parquet(input_path):
    """Load a DataFrame from a parquet file"""
    return pd.read_parquet(input_path)


## Main functions
def cross_validation(input_path, model_name, config_path='data/test/config.json', cv=5, n_jobs=1, export_preds_path=None):
    """Cross-validate a model with a dataset"""
    #########################################################
    click.secho("\nLoad and prepare data...\n", bold=True)
    ## Load data
    data_config = load_json(config_path)
    df = _load_parquet(input_path)
    ## Prepare data
    X, y = _prepare_input(df, target=data_config['target'])
    #########################################################
    click.secho("Load model...\n", bold=True)
    ## Load model
    model, fit_params = load_model(model_name)
    #########################################################
    click.secho(f"Run cross-validation and compute metrics with the model: {model_name}...\n", bold=True)
    ## Cross-validation
    y_hat = cross_val_predict(model, X, y, cv=cv, fit_params=fit_params, n_jobs=n_jobs)
    preds = pd.DataFrame({'y_true':y, 'y_hat':y_hat})
    ## Calculate metrics
    _metrics = _calculate_metrics(preds, metrics)
    #########################################################
    ## Export predictions
    if export_preds_path is not None:
        export_path = "data/processed" / Path(export_preds_path)
        click.secho(f"Export predictions in the folder: {export_path}...\n", bold=True)
        preds.to_csv(export_path, index=False)
    ## Metadata
    time = dt.now().strftime("%Y-%m-%d %H-%M-%S")
    metadata_output = Path(f"reports/metadata_{model_name}_{time}.txt")
    metadata = {
        'date': time,
        'n_jobs': n_jobs,
        'model name': model_name,
        'nbs of obs': X.shape[0],
        'nbs of features': X.shape[1],
        'percentage of positive target': np.round(np.mean(y.astype(int)), 2)
    }
    click.secho(f"Export metadata to the folder: {metadata_output}...\n", bold=True)
    with open(metadata_output, 'w') as file:
        file.write(f"Metadata:\n")
        file.write("----------------------------------\n")
        for k, v in metadata.items():
            file.write(f"{k}: {v}")
            file.write("\n")
        file.write("\nMetrics:\n")
        file.write("----------------------------------\n")
        for k, v in _metrics.items():
            file.write(f"{k}: {v}\n")
    

if __name__ == '__main__':
    cross_validation(input_path='data/interim/trainset_2019-10-28.parquet', model_name='basic_logit', export_preds_path='preds.csv')