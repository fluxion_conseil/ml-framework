# -*- coding: utf-8 -*-
import click
import logging
import pandas as pd
import json
import numpy as np
import sys

sys.path.append("src")

from utils import load_json

from pathlib import Path
from pandas.api.types import is_categorical_dtype, is_numeric_dtype, is_datetime64_any_dtype
from datetime import datetime as dt
from sklearn.model_selection import train_test_split

# Helper functions
def _fix_dtypes(df, numerical_cols, cat_cols, date_cols):
    """Check the types of the columns in the DataFrame. When necessary, the types a conversion is performed.
    
    Args:
        df: pandas DataFrame
            Input table loaded.
        numerical_cols: list
            A list of the columns that should be numerical.
        cat_cols: list
            A list of the columns that should be categorical.
        date_cols: list
            A list of the columns that should be datetime.
    
    Returns:
        df: pandas DataFrame
            The modify DataFrame with the right types.
    """
    df = df.copy(deep=True)

    inputed_columns = {*numerical_cols, *cat_cols, *date_cols}
    missing_columns = inputed_columns.difference(df.columns)
    unk_columns = df.columns.difference(inputed_columns)
    # Verify columns
    assert len(missing_columns) == 0, f"The features {missing_columns} are missings in the input table."
    click.secho(f"The features {unk_columns} are not known in the input table.")
    # Numerical data types
    if len(numerical_cols) != 0:
        df[numerical_cols] = df[numerical_cols].apply(pd.to_numeric, errors='raise')
    # Categorical data types
    if len(cat_cols) != 0:
        df[cat_cols] = df[cat_cols].apply(pd.Categorical)
    # Datetime data types
    if len(date_cols) != 0:
        for col, date_format in date_cols.items():
            df[col] = pd.to_datetime(df[col], format=date_format)
    return df


# Main functions
def make_dataset(input_filepath, config_path, output_path, test_size=0.33):
    """Create the input dataset for the ML problem."""
    ## Timestamp
    timestamp = dt.now().strftime("%Y-%m-%d")

    ## Load dataset
    cols_dict = load_json(config_path)
    df = pd.read_csv(input_filepath, index_col=cols_dict['index_col'])
    df_fixed = _fix_dtypes(df, cols_dict['numerical_cols'], cols_dict['cat_cols'], cols_dict['date_cols'])

    ## Train/test split
    train_df, test_df = train_test_split(df_fixed, test_size=test_size, random_state=42)

    ## Metadata
    metadata = {
        'input_path': input_filepath,
        'config_path': config_path,
        'output_path': output_path,
        'test_size':str(test_size),
        'timestamp': timestamp
    }
    metadata_path = Path(output_path) / f'metadata_{timestamp}.txt'
    with open(metadata_path, 'w') as file:
        for k, v in metadata.items():
            file.write(f"{k}: {v}")
            file.write("\n")
    
    ## Export dataset to parquet
    output_path_train = Path(output_path) / f"trainset_{timestamp}.parquet"
    train_df.reset_index(drop=True).to_parquet(output_path_train, compression='UNCOMPRESSED', engine='auto')
    output_path_test = Path(output_path) / f"testset_{timestamp}.parquet"
    test_df.reset_index(drop=True).to_parquet(output_path_test, compression='UNCOMPRESSED', engine='auto')
    


@click.command()
@click.option('-i', '--input_filepath', type=click.Path(exists=True))
@click.option('-c', '--config_path', type=click.Path(exists=True))
@click.option('-o', '--output_path', type=click.Path(exists=True))
@click.option('-t', '--test_size', type=float, default=0.33)
def make_dataset_cli(input_filepath, config_path, output_path, test_size):
    """Create the input dataset from the command line."""
    make_dataset(input_filepath, config_path, output_path)


if __name__ == '__main__':
    make_dataset_cli()
