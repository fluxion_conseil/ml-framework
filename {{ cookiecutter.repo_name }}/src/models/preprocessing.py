# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np

from sklearn.base import BaseEstimator, TransformerMixin

# Custom sklearn transformer
class SelectColumns(BaseEstimator, TransformerMixin):
    """Select columns"""
    def __init__(self, columns):
        self.columns = columns
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        return(X.loc[:, self.columns])

