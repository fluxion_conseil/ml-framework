from sklearn.base import BaseEstimator, ClassifierMixin
from scipy import stats

class ModeClassifier(BaseEstimator, ClassifierMixin):
    """Returns the mode of the target variable."""
    def fit(self, X, y=None):
        self.mode = stats.mode(y).mode[0]
        return(self)
    
    def predict(self, X, y=None):
        try:
            getattr(self, 'mode')
        except AttributeError:
            raise RuntimeError("The classifier must be train before predicting on new data.")
        return([self.mode for x in range(X.shape[0])])

model = ModeClassifier()

fit_params = {}