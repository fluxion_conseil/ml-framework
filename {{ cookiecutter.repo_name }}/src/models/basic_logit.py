import pandas as pd
import numpy as np
import sys

sys.path.append("src/")

from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline, FeatureUnion, make_pipeline, make_union
from sklearn.preprocessing import OneHotEncoder, FunctionTransformer
from sklearn.impute import SimpleImputer

from utils import load_json
from models.preprocessing import SelectColumns

# load cat/num cols
cols_dict = load_json('data/test/config.json')

# Preprocessing categorical pipeline
_categorical_preprocessor = make_pipeline(SelectColumns(cols_dict['cat_cols']), 
                                          SimpleImputer(strategy='most_frequent'),
                                          OneHotEncoder())

# Preprocessing numerical pipeline
_numerical_preprocessor = make_pipeline(SelectColumns(cols_dict['numerical_cols']),
                                        SimpleImputer(strategy='mean'))

# Preprocessor
_preprocessor = make_union(_categorical_preprocessor, _numerical_preprocessor)

# Classifier
_classifier = LogisticRegression(solver='liblinear')

# Model
model = make_pipeline(_preprocessor, _classifier)

fit_params = {}