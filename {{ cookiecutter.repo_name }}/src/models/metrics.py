## import metrics from sklearn
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score

metrics = {
    "recall":recall_score,
    "precision":precision_score,
    "accuracy":accuracy_score,
    "f1":f1_score
}